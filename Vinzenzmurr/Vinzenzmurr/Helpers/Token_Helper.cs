﻿//
// Token_Helper.cs
// Vinzenzmurr
//
// Created by Ghassen KHALFALLAH on 14/11/2017.
// Copyright © 2017 Yanogo.All rights reserved.
//
using Acr.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Models;
using Vinzenzmurr.Service;

namespace Vinzenzmurr.Helpers
{
    /// <summary>
    /// This Class is for manipulating The access token used in different webservice Calls (storing the token locally,Getiing the token ..)
    /// </summary>
   public static class Token_Helper
    {
        /// <summary>
        /// It stores The Token and Client informations locally
        /// </summary>
        /// <param name="token">A token object</param>
        /// <param name="client">A client object</param>
        public static void SaveToken(Token token,Client client)
        {
            Acr.Settings.Settings.Current.Set("Token", token);
            Acr.Settings.Settings.Current.Set("Client", client);
        }

        /// <summary>
        /// This method has a role to return the access token used in different web service calls.
        /// There is a verification on the access token expiration date .
        /// </summary>
        /// <returns>The stored Access token if it's date is still not expired , else it calls a refresh token service and return the new result</returns>
        public async static Task<Token> GetToken()
        {
            Token Actual_Token = Acr.Settings.Settings.Current.Get<Token>("Token");
            Client Session_Client = Acr.Settings.Settings.Current.Get<Client>("Client");
            var tokenTime = DateTime.Parse(Actual_Token.Access_token_expires_at);
            var NowTime = DateTime.Now;
            int result = DateTime.Compare(tokenTime, NowTime);
            if (result > 0)
            {
                return Actual_Token;
            }
            else
            {
                Authentication_Service Auth_Service = new Authentication_Service();
                return await Auth_Service.Refresh_Token(Session_Client.Client_id, Session_Client.Client_secret, Actual_Token.Refresh_token);
               
            }

        }
    }
}
