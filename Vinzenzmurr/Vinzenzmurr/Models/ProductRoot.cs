﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class ProductRoot
    {
        public string status { get; set; }
        public int code { get; set; }
        public List<Product> data { get; set; }
        public string name { get; set; }
    }
}
