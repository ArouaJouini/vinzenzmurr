﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class DataRoot
    {
        public string Access_token { get; set; }
        public string Refresh_token { get; set; }
        public DateTime Access_token_expires_at { get; set; }
        public DateTime Refresh_token_expires_at { get; set; }
    }
}
