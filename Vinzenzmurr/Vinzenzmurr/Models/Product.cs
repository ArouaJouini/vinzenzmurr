﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Ordering_Quantity { get; set; }
        public long Plu { get; set; }
        public long Price { get; set; }
        public string Selling_quantity { get; set; }
        public int Order { get; set; }

        public int New_Mon_Order { get; set; }
        public int New_Tue_Order { get; set; }
        public int New_Wen_Order { get; set; }
        public int New_Thu_Order { get; set; }
        public int New_Fri_Order { get; set; }



        public List<Order> Orders { get; set; }

        public Order mon_Order
        {
            get
            {
                return Orders[0];
            }
            set
            {
            }
        }
        public Order tue_Order
        {
            get
            {
                return Orders[1];
            }
            set
            {
            }
        }
        public Order wen_Order
        {
            get
            {
                return Orders[2];
            }
            set
            {
            }
        }
        public Order thu_Order
        {
            get
            {
                return Orders[3];
            }
            set
            {
            }
        }
        public Order fri_Order
        {
            get
            {
                return Orders[4];
            }
            set
            {
            }
        }

    }

    public class Order
    {
        public string Date { get; set; }
        public long Quantity { get; set; }
    }

}
