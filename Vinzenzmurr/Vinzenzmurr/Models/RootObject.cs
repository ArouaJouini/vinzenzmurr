﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class RootObject
    {
        public string Status { get; set; }
        public int Code { get; set; }
        public Data Data { get; set; }
        public string Name { get; set; }
    }
}
