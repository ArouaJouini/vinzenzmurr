﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
   public class OrderRoot
    {
        public string Status { get; set; }
        public int Code { get; set; }
        public List<Order_Product> data { get; set; }
        public string Name { get; set; }
    }
}
