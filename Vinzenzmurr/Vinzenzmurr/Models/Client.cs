﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class Client
    {
        public string Client_id { get; set; }
        public string Client_secret { get; set; }
    }
}
