﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class Token
    {
        public string Access_token { get; set; }
        public string Refresh_token { get; set; }
        public string Access_token_expires_at { get; set; }
        public string Refresh_token_expires_at { get; set; }
    }
}
