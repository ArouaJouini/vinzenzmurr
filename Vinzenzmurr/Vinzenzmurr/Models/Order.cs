﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
   public class Order_Product
    {
        public long product_id { get; set; }
        public string date { get; set; }
        public long quantity { get; set; }

    }
}
