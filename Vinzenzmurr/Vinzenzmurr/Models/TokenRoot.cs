﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
   public class TokenRoot
    {
            public string Status { get; set; }
            public int Code { get; set; }
            public Token Data { get; set; }
            public string Name { get; set; }
    }
}
