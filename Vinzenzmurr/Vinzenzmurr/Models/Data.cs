﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
    public class Data
    {
        public Client Client { get; set; }
        public User User { get; set; }
        public Token Token { get; set; }
        public Settings Settings { get; set; }
    }
}
