﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinzenzmurr.Models
{
   public class Order_History
    {
        public long Code { get; set; }
        public List<HistoryData> Data { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }

    public class HistoryData
    {
        public string Date { get; set; }
        public List<Stat> Stats { get; set; }

        public string Stats0
        {
            get
            {
                return Stats[0].Nbr + " " + "Produkte";
            }
            set
            {
            }
        }
        public string Stats1
        {
            get
            {
                return Stats[1].Nbr + " " + "Produkte";
            }
            set
            {
            }
        }
        public string Stats2
        {
            get
            {
                return Stats[2].Nbr + " " + "Produkte";
            }
            set
            {
            }
        }
        public string Stats3
        {
            get
            {
                return Stats[3].Nbr + " " + "Produkte";
            }
            set
            {
            }
        }
        public string Stats4
        {
            get
            {
                return Stats[4].Nbr + " " + "Produkte";
            }
            set
            {
            }
        }

        public string Stats5
        {
            get
            {
                return  "0 " + "Produkte";
            }
            set
            {
            }
        }

    }

    public class Stat
    {
        public string Category { get; set; }
        public long Nbr { get; set; }
    }
}
