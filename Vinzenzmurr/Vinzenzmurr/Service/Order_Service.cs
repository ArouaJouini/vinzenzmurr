﻿using Newtonsoft.Json;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Helpers;
using Vinzenzmurr.Models;

namespace Vinzenzmurr.Service
{

   public class Order_Service
    {
        string BaseUrl = Constants.WebServiceUrl;
        RestClient client;
        Token Token;

        public Order_Service()
        {
            // Instanciating out HTTP Client using our Base URL
            client = new RestClient(BaseUrl);
            //Getting our Access Token
            Get_Token();
        }

        /// <summary>
        /// Method To get the stored Token , if it is expired it will call the refresh token service and return the new token
        /// </summary>
        public async void Get_Token()
        {
            Token = await Token_Helper.GetToken();
        }


        public async Task<OrderRoot> PostOrder(List<Order_Product> Orders)
        {
            try
            {
                //Specify The ressource we are querying via the url part, and the method type
                var request = new RestRequest("v1/vm/orders", Method.POST);
                //Add the Token in Header
                request.AddParameter("Authorization", "Bearer " + Token.Access_token, ParameterType.HttpHeader);
                //Add the Body // RestSharp will serialize it to a JSON Body
                request.AddBody(Orders);
                // execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                //Deserialise the result into our Product Root Object
                var Result = JsonConvert.DeserializeObject<OrderRoot>(content);
                return Result;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }


        public async Task<Order_History> GetOrderHistory()
        {
            try
            {
                //Specify The ressource we are querying via the url part, and the method type
                var request = new RestRequest("v1/vm/orders/history", Method.GET);
                //Add the Token in Header
                request.AddParameter("Authorization","Bearer " + Token.Access_token, ParameterType.HttpHeader);
                // execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                //Deserialise the result into our Product Root Object
                var Result = JsonConvert.DeserializeObject<Order_History>(content);
                return Result;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }

    }
}
