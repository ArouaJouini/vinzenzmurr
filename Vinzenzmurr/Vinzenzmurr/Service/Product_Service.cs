﻿//
// Product_Service.cs
// Vinzenzmurr
//
// Created by Ghassen KHALFALLAH on 14/11/2017.
// Copyright © 2017 Yanogo.All rights reserved.
//
using Newtonsoft.Json;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Helpers;
using Vinzenzmurr.Models;

namespace Vinzenzmurr.Service
{
   public class Product_Service
    {
        string BaseUrl = Constants.WebServiceUrl;
        RestClient client;
        Token Token;
        public  Product_Service()
        {
            // Instanciating out HTTP Client using our Base URL
            client = new RestClient(BaseUrl);
            //Getting our Access Token
            Get_Token();
          
        }

        /// <summary>
        /// Method To get the stored Token , if it is expired it will call the refresh token service and return the new token
        /// </summary>
        public async void Get_Token()
        {
            Token = await  Token_Helper.GetToken();
        }

        public async Task<ProductRoot> GetProducts(string category,string subCategory)
        {
            try
            {
                //Specify The ressource we are querying via the url part, and the method type
                var request = new RestRequest("v1/vm/products", Method.GET);
                //Add the Token in Header
                request.AddParameter("Authorization", "Bearer " + Token.Access_token, ParameterType.HttpHeader);
                //Add the parameters
                request.AddParameter("category", category);
                request.AddParameter("sub_category", subCategory);
                // execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                //Deserialise the result into our Product Root Object
                var Result = JsonConvert.DeserializeObject<ProductRoot>(content);
                return Result;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }

        public async Task<ProductRoot> SearchProducts(string filter)
        {
            try
            {
                //Specify The ressource we are querying via the url part, and the method type
                var request = new RestRequest("v1/vm/products/search", Method.GET);
                //Add the Token in Header
                request.AddParameter("Authorization", "Bearer " + Token.Access_token, ParameterType.HttpHeader);
                //Add the parameters
                request.AddParameter("filter", filter);
                // execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                //Deserialise the result into our Product Root Object
                var Result = JsonConvert.DeserializeObject<ProductRoot>(content);
                return Result;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }
    }
}
