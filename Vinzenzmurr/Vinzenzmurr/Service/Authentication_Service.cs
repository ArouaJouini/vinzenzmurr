﻿//
// Authentication_Service.cs
// Vinzenzmurr
//
// Created by Ghassen KHALFALLAH on 14/11/2017.
// Copyright © 2017 Yanogo.All rights reserved.
//
using Newtonsoft.Json;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Helpers;
using Vinzenzmurr.Models;

namespace Vinzenzmurr.Service
{
   public class Authentication_Service
    {
        string BaseUrl = Constants.WebServiceUrl;
        RestClient client;
        //For Testing we are using this shop Name
        string VMshop = "002SCHE";

        public Authentication_Service()
        {
            // Instanciating out HTTP Client using our Base URL
            client = new RestClient(BaseUrl);
        }

        /// <summary>
        /// The login method ,it uses the VMShop name as a parameter and return 
        /// </summary>
        /// <returns></returns>
        public async Task<RootObject> Login()
        {
            try
            {
                //Specify The ressource we are querying via the url part, and the method type
                var request = new RestRequest("v1/vm/login", Method.POST);
                //Add the VMshop name as a parameter
                request.AddParameter("name", VMshop);
                // execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                //Deserialise the result into our Root Object
                var Result = JsonConvert.DeserializeObject<RootObject>(content);
                //Save the Token and client informations
                Token_Helper.SaveToken(Result.Data.Token, Result.Data.Client);

                return Result;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// The method for refreshing The access token.
        /// </summary>
        /// <param name="client_Id"></param>
        /// <param name="client_secret"></param>
        /// <param name="refresh_token"></param>
        /// <returns>The new access token</returns>
        public async Task<Token> Refresh_Token(string client_Id, string client_secret, string refresh_token)
        {
            try
            {
                var request = new RestRequest("v1/vm/auths/token", Method.POST);

                //Add the parameters
                request.AddParameter("grant_type", "refresh_token");
                request.AddParameter("client_id", client_Id);
                request.AddParameter("client_secret", client_secret);
                request.AddParameter("refresh_token", refresh_token);
                // Execute the request
                var response = await client.Execute(request);
                var content = response.Content;
                var Result = JsonConvert.DeserializeObject<TokenRoot>(content);
                return Result.Data;
            }
            catch (Exception ex)
            {
                var Error_msg = ex.Message;
                return null;
            }
        }

    }
}
