﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Popups;

namespace VinzenzmurrApp.service
{
    public class NetworkAvailability
    {
        public static bool IsInternet()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            bool isInternetConnected = NetworkInterface.GetIsNetworkAvailable();
            if (profile != null && isInternetConnected)
                return true;
            else
            {
                //Display Alert when there's no internet connection on the device 
                var dialog = new MessageDialog("Cannot connect to server! Please check your internet connection and try again", "Error");
                dialog.Options = MessageDialogOptions.None;
                //OK Button
                UICommand okBtn = new UICommand("OK");
                dialog.Commands.Add(okBtn);
                return false;
            }
        }
    }
}

