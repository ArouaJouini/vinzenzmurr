﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Models;

namespace VinzenzmurrApp.Helpers
{
    public class ClientHelper
    {
        public string client_Id = "";
        public string client_secret = "";
        public string refresh_token = "";
        public string access_token = "";
        public static HttpClient GetClient(string username, string password)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));

            var client = new HttpClient()
            {
                DefaultRequestHeaders = { Authorization = authValue }
                //Set some other client defaults like timeout / BaseAddress
            };
            return client;
        }
        public static HttpClient GetClient(string token)
        {
            //var authValue = new AuthenticationHeaderValue("Bearer ", token);

            var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return client;
        }
        public async Task<string> Connecting()
        {
            Uri requestUri = new Uri("http://82.165.72.79/api/v1/vm/login");//replace your Url 
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.name = "002SCHE";
            string VMshop = "";
            VMshop = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            var objClint = new System.Net.Http.HttpClient();
            System.Net.Http.HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent(VMshop.ToString(), System.Text.Encoding.UTF8, "application/json"));
            string responJsonText = await respon.Content.ReadAsStringAsync();
            var responseJson = JsonConvert.DeserializeObject<RootObject>(responJsonText);
            client_Id = responseJson.Data.Client.Client_id;
            client_secret = responseJson.Data.Client.Client_secret;
            refresh_token = responseJson.Data.Token.Refresh_token;
            return access_token = responseJson.Data.Token.Access_token;
        }
        public static string RefreshToken(string client_Id, string client_secret, string refresh_token)
        {
            HttpClient client = new HttpClient();
            var pairs = new List<KeyValuePair<string, string>>();

            pairs.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            pairs.Add(new KeyValuePair<string, string>("client_id", client_Id));
            pairs.Add(new KeyValuePair<string, string>("client_secret", client_secret));
            pairs.Add(new KeyValuePair<string, string>("refresh_token", refresh_token));
            var content = new FormUrlEncodedContent(pairs);
            HttpResponseMessage response = client.PostAsync("http://82.165.72.79/api/v1/vm/auths/token", content).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            var tokenDictionary = JsonConvert.DeserializeObject<TokenRoot>(result);
            string token = tokenDictionary.Data.Access_token;
            return token;
        }
    }
}
