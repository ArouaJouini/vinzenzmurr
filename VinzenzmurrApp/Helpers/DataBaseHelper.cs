﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Vinzenzmurr.Models;
using Windows.Storage;
namespace VinzenzmurrApp.Helpers
{
    public class DataBaseHelper
    {
        public static string DbName = "ProductOrder.sqlite";
        public static string DbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, DbName);
        public async static Task<bool> CreateDatabase()
        {
            var result = await Checkdatabase();
            if (!result)
            {
                //Creating a database
                // var connection = new SQLiteConnection(DbPath);
                SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new
                          SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), DbPath);
                {
                    //Creating a table
                    connection.RunInTransaction(() =>
                    {
                        connection.CreateTable<Product>();
                    });
                }

                connection.Close();
                return true;
            }
            else
                return false;
        }
        public static async Task<bool> Checkdatabase()
        {
            var dbexist = true;
            try
            {
                var storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync(DbName);
                if (storageFile == null) dbexist = false;
            }
            catch (Exception ex)
            {
                dbexist = false;
            }
            return dbexist;
        }
        public static async Task<bool> DeleteDatabase()
        {
            try
            {
                var storageFile = await StorageFile.GetFileFromPathAsync(DbPath);
                await storageFile.DeleteAsync(StorageDeleteOption.PermanentDelete);
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        public static void AddNewProductOrder(Product obj)
        {
            SQLite.Net.SQLiteConnection connection;
            using (connection = new SQLite.Net.SQLiteConnection(new
                          SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), DbPath))
            {
                connection.RunInTransaction(() =>
                {
                    connection.Insert(obj);
                });
            }
            connection.Close();
        }
        public IEnumerable<Product> GetAllProducts()
        {
            SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new
                          SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), DbPath);
            return (from t in connection.Table<Product>() select t).ToList();
        }
        public void GetProduct(string query)
        {
            SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new
                          SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), DbPath);
            var result = connection.Table<Product>().FirstOrDefault(t => t.Name == query);
        }
    }
}
