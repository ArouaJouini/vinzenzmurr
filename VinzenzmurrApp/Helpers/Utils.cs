﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vinzenzmurr.Models;

namespace VinzenzmurrApp.Helpers
{
   public static class Utils
    {

        /// <summary>
        /// To check if a given string contains all or part of the second given string , with ignore case
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static bool _Contains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

     static  string listorder =  Acr.Settings.Settings.Current.Get<string>("Orders");

        public static List<Product> OrderList = listorder != null ? JsonConvert.DeserializeObject<List<Product>>(listorder) : new List<Product>();
    }
}
