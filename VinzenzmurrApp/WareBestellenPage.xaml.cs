﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Security.Authentication.Web;
using Windows.Security.Credentials;
using Windows.Data.Json;
using System.Text;
using System.Dynamic;
using Windows.Web.Http;
using System.Net.Http.Headers;
using VinzenzmurrApp.Helpers;
using Vinzenzmurr.Service;
using Vinzenzmurr.Models;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VinzenzmurrApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WareBestellenPage : Page
    {
        Token stored_Token = Acr.Settings.Settings.Current.Get<Token>("Token");
        string storedToken;
        string storedCategory = "";
        string storedSubCategory = "";
        RootObject rootobject;
        Product_Service product_service;
        //We used Observable Collections so that the listview will be always notified if any 
        //changes are made to the source .
        //Ghassen Khalfallah 10/11/2017
        public static ObservableCollection<Vinzenzmurr.Models.Product> Product_List { get; set; }
        public ObservableCollection<Vinzenzmurr.Models.Product> Searched_Product_List { get; set; }
        public WareBestellenPage()
        {
            this.InitializeComponent();
            var color = GetSolidColorBrush("#b8432e").Color;
            App.Current.Resources["SystemControlHighlightListAccentLowBrush"] = new SolidColorBrush(color);
            App.Current.Resources["SystemControlHighlightListAccentMediumBrush"] = new SolidColorBrush(color);

            App.Current.Resources["SystemControlHighlightBaseHighBrush"] = new SolidColorBrush(color);
            POSTreq();
            NavigationCacheMode = NavigationCacheMode.Enabled;
            storedToken = stored_Token.Access_token;
            test.Click += PivotItemClick;
            test2.Click += PivotItemClick;
        }

        public SolidColorBrush GetSolidColorBrush(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = 0;  //(byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            SolidColorBrush myBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(a, r, g, b));
            return myBrush;
        }
        public async void POSTreq()
        {
            Authentication_Service Auth_Service = new Authentication_Service();


            var menuItem = await Auth_Service.Login();
            var List = menuItem.Data.Settings.Categories[0].Subcategories;
            subCategoryMenuList.ItemsSource = menuItem.Data.Settings.Categories[0].Subcategories;
            textCat.Text = menuItem.Data.Settings.Categories[0].Name;
            sb.Text = menuItem.Data.Settings.Categories[1].Name;
            progressRing.IsActive = false;
            txtProgress.Visibility = Visibility.Collapsed;
            storedCategory = menuItem.Data.Settings.Categories[0].Slug;
            storedSubCategory = menuItem.Data.Settings.Categories[0].Subcategories[1].Slug;
            rootobject = menuItem;
            //////******//////
            //This is just for testing
            //Need to clearify and make the process Later
            //Ghassen Khalfallah
            //////////////////////
            var sub = "schweinefleisch-100";
            storedCategory = "fleisch-wurst";
            product_service = new Product_Service();
            ProductRoot List_To_Show = await product_service.GetProducts(storedCategory, sub);
            Product_List = new ObservableCollection<Product>(List_To_Show.data);
            listView.ItemsSource = Product_List;
        }
        private void back_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();

        }

        private void fleichButton_Click(object sender, RoutedEventArgs e)
        {
            var isclicked = true;
            MySplitView.IsPaneOpen = true;
            IsClicked(isclicked);
            subCategoryMenuList.ItemsSource = rootobject.Data.Settings.Categories[0].Subcategories;
            storedCategory = rootobject.Data.Settings.Categories[0].Slug;
            isclicked = false;
        }
        private void IsClicked(bool isClicked)
        {
            if (isClicked)
            {
                fleichButton.Background = new SolidColorBrush(color: Colors.Gray);
                sbButton.Background = new SolidColorBrush(color: Colors.Transparent);
            }
            else
                fleichButton.Background = new SolidColorBrush(color: Colors.Transparent);
        }
        //private async void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (selectedComboBoxItem == null) return;
        //    var combo = (ComboBox)sender;
        //    var item = (ComboBoxItem)combo.SelectedItem;
        //    var sub = item.ToString();
        //    var client = new System.Net.Http.HttpClient();
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
        //    Uri geturi = new Uri("http://82.165.72.79/api/v1/vm/products?category=" + storedCategory + "&sub_category=" + sub);
        //    System.Net.Http.HttpResponseMessage responseGet = await client.GetAsync(geturi);
        //    string response = await responseGet.Content.ReadAsStringAsync();
        //    var productItem = JsonConvert.DeserializeObject<ProductRoot>(response);
        //    listView.ItemsSource = productItem.data;
        //}

        private void listView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Vinzenzmurr.Models.Product item = e.ClickedItem as Vinzenzmurr.Models.Product;
            Frame.Navigate(typeof(ProductDetail), item);
        }

        private async void addButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<string> strings = new ObservableCollection<string>() { "First" };
            ListView inputTextBox = new ListView();
            TextBox tt = new TextBox();
            Grid myGrid = new Grid();
            inputTextBox.ItemTemplate = this.Resources["myResourceTemplate"] as DataTemplate;
            inputTextBox.ItemsSource = strings;
            myGrid.Children.Add(inputTextBox);
            ContentDialog dialog = new ContentDialog();
            dialog.Content = myGrid;
            dialog.Title = "Liefrung Plannen";
            dialog.IsSecondaryButtonEnabled = true;
            dialog.Background = new SolidColorBrush(color: Colors.White);

            dialog.PrimaryButtonText = "Abbrechen";
            dialog.SecondaryButtonText = "Liefrung ubernehemen";
            await dialog.ShowAsync();
            //if (await dialog.ShowAsync() == ContentDialogResult.Primary)
            //   // return inputTextBox.Text;
            //else
            //    //return "";
            //var confirmOrderDialog = new Windows.UI.Popups.MessageDialog(
            //    "Bitte betatigen Sie Ihre Bestellung um  20:00 Uhr wird sie automatisch ubernommen und ist somit gultig.",
            //    "Bestellen");
            //var txt = new TextBox { Width = 60 };
            //confirmOrderDialog.Content = txt;
            //confirmOrderDialog.Commands.Add(new Windows.UI.Popups.UICommand("Bestatigen") { Id = 0 });
            //// add another button if you want to
            //confirmOrderDialog.Commands.Add(new Windows.UI.Popups.UICommand("Abbrechen") { Id = 1 });
            ////ContentDialogResult result = await confirmOrderDialog.ShowAsync();
            //var result = await confirmOrderDialog.ShowAsync();
            //confirmOrderDialog.DefaultCommandIndex = 0;
            //// Confirm the order if the user clicked the primary button.
            ///// Otherwise, do nothing.
            //if ((int)result.Id == 0)
            //{

            //}
            //else
            //{
            //    // The user clicked the CancelButton, pressed ESC, Gamepad B, or the system back button.
            //    // Do nothing.
            //}
            //var item = (sender as FrameworkElement).Tag as Vinzenzmurr.Models.Product;
            //var ind = listView.Items.IndexOf(item);

            ////Add/update the item in the list to be delivered
            //var listtoadd = Helpers.Utils.OrderList;
            //var isexistant = listtoadd.IndexOf(item) > -1;
            //if (!isexistant)
            //{
            //    item.Order = item.Order + 1;
            //    listtoadd.Add(item);
            //}
            //else
            //{
            //    int index = listtoadd.IndexOf(item);
            //    item.Order = item.Order + 1;
            //    listtoadd[index] = item;

            //}
            ////update the textbox
            //Product_List[ind] = item;
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as FrameworkElement).Tag as Vinzenzmurr.Models.Product;
            var ind = listView.Items.IndexOf(item);

            //Delete/update the item in the list to be delivered
            var listtoadd = Helpers.Utils.OrderList;
            var isexistant = listtoadd.IndexOf(item) > -1;
            if (isexistant)
            {
                int index = listtoadd.IndexOf(item);
                if (item.Order != 0)
                {
                    item.Order = item.Order - 1;
                    if (item.Order == 0)
                    {
                        listtoadd.RemoveAt(index);
                    }
                    //update the textbox
                    Product_List[ind] = item;
                }

            }


        }

        private void sbButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = true;
            fleichButton.Background = new SolidColorBrush(color: Colors.Transparent);
            sbButton.Background = new SolidColorBrush(color: Colors.Gray);
            subCategoryMenuList.ItemsSource = rootobject.Data.Settings.Categories[1].Subcategories;
            storedCategory = rootobject.Data.Settings.Categories[1].Slug;
        }
        private async void txt_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var item = (TextBlock)sender;
            var sub = item.Text;
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
            Uri geturi = new Uri("http://82.165.72.79/api/v1/vm/products?category=" + storedCategory + "&sub_category=" + sub);
            System.Net.Http.HttpResponseMessage responseGet = await client.GetAsync(geturi);
            string response = await responseGet.Content.ReadAsStringAsync();
            var productItem = JsonConvert.DeserializeObject<ProductRoot>(response);
            Product_List = new ObservableCollection<Product>(productItem.data);
            listView.ItemsSource = Product_List;
        }

        private void arrow_Click(object sender, RoutedEventArgs e)
        {
            if (MySplitView.IsPaneOpen == true)
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-right.png"));
            else
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-left.png"));
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        /// <summary>
        /// Search method for updating the list , needs an action from the user (Taping the text + enter button )
        /// Created By Ghassen Khalfallah on 10/11/2017
        /// </summary>
        private async void searchBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            string SearchText;
            //This is for the suggestion List(Not included in our case for now)
            if (args.ChosenSuggestion != null)
            {
                SearchText = args.ChosenSuggestion.ToString();
            }
            else
            {
                SearchText = sender.Text;
            }


            //If the search box contains a text then we will search for it , if it is empty we will leave the initial list
            if (!string.IsNullOrEmpty(SearchText))
            {
                Vinzenzmurr.Models.ProductRoot List_To_Show = await product_service.SearchProducts(SearchText);
                Searched_Product_List = new ObservableCollection<Vinzenzmurr.Models.Product>(List_To_Show.data);
                listView.ItemsSource = Searched_Product_List;
                if (MySplitView.IsPaneOpen == true)
                {
                    imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-right.png"));
                    MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
                }

            }
            else
            {
                listView.ItemsSource = Product_List;
            }
        }

        private void bestellungButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Actual_Order));
            bestellungButton.Background = new SolidColorBrush(color: Colors.Gray);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (MySplitView.IsPaneOpen == true)
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-right.png"));
            else
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-left.png"));
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            test.BorderBrush = new SolidColorBrush(color: Colors.Green);
            test.BorderThickness = new Thickness(0, 0, 0, 2);
            test2.BorderBrush = new SolidColorBrush(color: Colors.White);
        }
        private void test2_Click(object sender, RoutedEventArgs e)
        {
            if (MySplitView.IsPaneOpen == true)
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-right.png"));
            else
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-left.png"));
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            test.BorderBrush = new SolidColorBrush(color: Colors.White);
            test2.BorderBrush = new SolidColorBrush(color: Colors.Green);
            test2.BorderThickness = new Thickness(0, 0, 0, 2);
        }
        public void PivotItemClick(object sender, RoutedEventArgs e)
        {
            if (MySplitView.IsPaneOpen == true)
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-right.png"));
            else
                imageButton.Source = new BitmapImage(new Uri(this.BaseUri, "Assets/ico-arrow-left.png"));
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            switch ((sender as Button).Name)
            {
                case "test":
                    test.BorderBrush = new SolidColorBrush(color: Colors.Green);
                    test.BorderThickness = new Thickness(0, 0, 0, 2);
                    test2.BorderBrush = new SolidColorBrush(color: Colors.White);
                    break;
                case "test2":
                    test.BorderBrush = new SolidColorBrush(color: Colors.White);
                    test2.BorderBrush = new SolidColorBrush(color: Colors.Green);
                    test2.BorderThickness = new Thickness(0, 0, 0, 2);
                    break;
                default:
                    break;
            }
        }
    }
}
