﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using Vinzenzmurr.Models;
using Vinzenzmurr.Service;
using VinzenzmurrApp.Helpers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VinzenzmurrApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Actual_Order : Page
    {
        string storedToken = "";
        string storedCategory = "";
        string storedSubCategory = "";
        RootObject rootobject;

        //We used Observable Collections so that the listview will be always notified if any 
        //changes are made to the source .
        //Ghassen Khalfallah 10/11/2017
        public ObservableCollection<Product> Product_List { get; set; }
        public ObservableCollection<Product> Searched_Product_List { get; set; }
        public ObservableCollection<Product> _Product_List { get; set; }
        private const string _listofOrders = "Ordersfile.txt";
        StorageFolder localFolder = ApplicationData.Current.LocalFolder;

        public Actual_Order()
        {
            this.InitializeComponent();
            App.Current.Resources["SystemControlHighlightListAccentLowBrush"] = new SolidColorBrush(Colors.Gray);
            App.Current.Resources["SystemControlHighlightListAccentMediumBrush"] = new SolidColorBrush(Colors.Gray);
            POSTreq();
            // NavigationCacheMode = NavigationCacheMode.Enabled;
        }
        public async void POSTreq()
        {
            Uri requestUri = new Uri("http://82.165.72.79/api/v1/vm/login");
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.name = "002SCHE";
            string VMshop = "";
            VMshop = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            var objClint = new System.Net.Http.HttpClient();
            System.Net.Http.HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent(VMshop.ToString(), System.Text.Encoding.UTF8, "application/json"));
            string responJsonText = await respon.Content.ReadAsStringAsync();
            var menuItem = JsonConvert.DeserializeObject<RootObject>(responJsonText);
            var List = menuItem.Data.Settings.Categories[0].Subcategories;
            //  subCategoryMenuList.ItemsSource = menuItem.Data.Settings.Categories[0].Subcategories;
            textCat.Text = menuItem.Data.Settings.Categories[0].Name;
            sb.Text = menuItem.Data.Settings.Categories[1].Name;
            progressRing.IsActive = false;
            txtProgress.Visibility = Visibility.Collapsed;
            storedToken = menuItem.Data.Token.Access_token;
            storedCategory = menuItem.Data.Settings.Categories[0].Slug;
            storedSubCategory = menuItem.Data.Settings.Categories[0].Subcategories[1].Slug;
            rootobject = menuItem;
            storedToken = ClientHelper.RefreshToken(rootobject.Data.Client.Client_id, rootobject.Data.Client.Client_secret, rootobject.Data.Token.Refresh_token);


            //////******//////
            //This is just for testing
            //Need to clearify and make the process Later
            //Ghassen Khalfallah
            //////////////////////
            //Waiting delivery ////
            var sub = "schweinefleisch-100";
            storedCategory = "fleisch-wurst";
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
            Uri geturi = new Uri("http://82.165.72.79/api/v1/vm/products?category=" + storedCategory + "&sub_category=" + sub);
            System.Net.Http.HttpResponseMessage responseGet = await client.GetAsync(geturi);
            string response = await responseGet.Content.ReadAsStringAsync();
            var productItem = JsonConvert.DeserializeObject<ProductRoot>(response);
            Product_List = new ObservableCollection<Product>(productItem.data);
            listView.ItemsSource = Product_List;
         
            //Actual delivery ////
            //var sub_ = "spanferkelteile-101";
            //storedCategory = "fleisch-wurst";
            //var client_ = new System.Net.Http.HttpClient();
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
            //Uri geturi_ = new Uri("http://82.165.72.79/api/v1/vm/products?category=" + storedCategory + "&sub_category=" + sub_);
            //System.Net.Http.HttpResponseMessage responseGet_ = await client.GetAsync(geturi_);
            //string response_ = await responseGet_.Content.ReadAsStringAsync();
            //var productItem_ = JsonConvert.DeserializeObject<ProductRoot>(response_);
            _Product_List = new ObservableCollection<Product>(Utils.OrderList.OrderBy(x => x.Id));

            listView2.ItemsSource = _Product_List;
        }
        private void back_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }



        private void fleichButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = true;
            // subCategoryMenuList.ItemsSource = rootobject.Data.Settings.Categories[0].Subcategories;
            storedCategory = rootobject.Data.Settings.Categories[0].Slug;
        }



        private void listView_ItemClick(object sender, ItemClickEventArgs e)
        {
           Product item = e.ClickedItem as Product;
            Frame.Navigate(typeof(ProductDetail), item);
        }

        private void sbButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = true;
            //  subCategoryMenuList.ItemsSource = rootobject.Data.Settings.Categories[1].Subcategories;
            storedCategory = rootobject.Data.Settings.Categories[1].Slug;
        }



        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            var entity = (sender as FrameworkElement).Tag as Product;
            var ind = listView2.Items.IndexOf(entity);

            //Add/update the item in the list to be delivered
            var listtoadd = Helpers.Utils.OrderList;
            var isexistant = listtoadd.IndexOf(entity) > -1;
            if (!isexistant)
            {
                entity.Order = entity.Order + 1;
                listtoadd.Add(entity);
            }
            else
            {
                int index = listtoadd.IndexOf(entity);
                entity.Order = entity.Order + 1;
                listtoadd[index] = entity;

            }
            //update the textbox
            _Product_List[ind] = entity;

            var Global_Product_List = WareBestellenPage.Product_List;
            isexistant = Global_Product_List.IndexOf(entity) > -1;
            if (isexistant)
            {

                int index = Global_Product_List.IndexOf(entity);
                Global_Product_List[index] = entity;
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            var entity = (sender as FrameworkElement).Tag as Product;
            var ind = listView2.Items.IndexOf(entity);

            //Add/update the item in the list to be delivered
            var listtoadd = Helpers.Utils.OrderList;
            var isexistant = listtoadd.IndexOf(entity) > -1;
            if (isexistant)
            {
                int index = listtoadd.IndexOf(entity);
                if (entity.Order != 0)
                {
                    entity.Order = entity.Order - 1;
                    if (entity.Order == 0)
                    {
                        listtoadd.RemoveAt(index);
                    }
                    //update the textbox
                    _Product_List[ind] = entity;
                }

            }

            var Global_Product_List = WareBestellenPage.Product_List;
            isexistant = Global_Product_List.IndexOf(entity) > -1;
            if (isexistant)
            {

                int index = Global_Product_List.IndexOf(entity);
                Global_Product_List[index] = entity;
            }

        }

        /// <summary>
        /// Search method for updating the list , needs an action from the user (Taping the text + enter button )
        /// Created By Ghassen Khalfallah on 10/11/2017
        /// </summary>
        private void searchBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            string SearchText;
            //This is for the suggestion List(Not included in our case for now)
            if (args.ChosenSuggestion != null)
            {
                SearchText = args.ChosenSuggestion.ToString();
            }
            else
            {
                SearchText = sender.Text;
            }


            //If the search box contains a text then we will search for it , if it is empty we will leave the initial list
            if (!string.IsNullOrEmpty(SearchText))
            {

                Searched_Product_List = new ObservableCollection<Product>(Product_List.Where(x => Utils._Contains(x.Name, SearchText, StringComparison.OrdinalIgnoreCase) || Utils._Contains(x.Plu.ToString(), SearchText, StringComparison.OrdinalIgnoreCase)));
                listView.ItemsSource = Searched_Product_List;
            }
            else
            {
                listView.ItemsSource = Product_List;
            }




        }

        private async void GetHistoryOrder()
        {
            //var client = new System.Net.Http.HttpClient();
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", storedToken);
            //Uri geturi = new Uri("http://82.165.72.79/api/v1/vm/orders/history");
            //System.Net.Http.HttpResponseMessage responseGet = await client.GetAsync(geturi);
            //string response = await responseGet.Content.ReadAsStringAsync();
            //var productItem = JsonConvert.DeserializeObject<HistoryRoot>(response);
            //var _History_List = new ObservableCollection<Datum>(productItem.Data);
            //listViewHistory.ItemsSource = _History_List;

            Order_Service OrderService = new Order_Service();
            var OrdersHistory = await OrderService.GetOrderHistory();
            var _History_List = new ObservableCollection<HistoryData>(OrdersHistory.Data);
            listViewHistory.ItemsSource = _History_List;

        }

        private void rootPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            var pivot = (PivotItem)(sender as Pivot).SelectedItem;
            switch (pivot.Header.ToString())
            {
                case "Jetzt bestellen":
                    ButtonsArea.Visibility = Visibility.Visible;
                    Yes.Visibility = Visibility.Visible;
                    break;
                case "Anstehende Lieferungen":
                    ButtonsArea.Visibility = Visibility.Collapsed;
                    Yes.Visibility = Visibility.Collapsed;
                    break;
                case "vergangene Bestellungen":
                    ButtonsArea.Visibility = Visibility.Collapsed;
                    GetHistoryOrder();
                    break;
            }
        }

        private async void Yes_Click(object sender, RoutedEventArgs e)
        {
            var confirmOrderDialog = new Windows.UI.Popups.MessageDialog(
                "Bitte betatigen Sie Ihre Bestellung um  20:00 Uhr wird sie automatisch ubernommen und ist somit gultig.",
                "Bestellen");
            confirmOrderDialog.Commands.Add(new Windows.UI.Popups.UICommand("Bestatigen") { Id = 0 });
            // add another button if you want to
            confirmOrderDialog.Commands.Add(new Windows.UI.Popups.UICommand("Abbrechen") { Id = 1 });
            //ContentDialogResult result = await confirmOrderDialog.ShowAsync();
            var result = await confirmOrderDialog.ShowAsync();
            confirmOrderDialog.DefaultCommandIndex = 0;
            // Confirm the order if the user clicked the primary button.
            /// Otherwise, do nothing.
            if ((int)result.Id == 0)
            {
                // Get Order and send list to local file.
                var productstoorder = JsonConvert.SerializeObject(_Product_List);
                Acr.Settings.Settings.Current.Set("Orders", productstoorder);

                Order_Service order_Service = new Order_Service();
                List<Order_Product> NewOrders = new List<Order_Product>();
                Order_Product New_Order;

                foreach (var Product in _Product_List)
                {
                    New_Order = new Order_Product { product_id = Product.Id, quantity = Product.Order, date = DateTime.Today.ToString("yyyy-M-dd") };
                    NewOrders.Add(New_Order);
                }

                var Result = await order_Service.PostOrder(NewOrders);
            }
            else
            {
                // The user clicked the CancelButton, pressed ESC, Gamepad B, or the system back button.
                // Do nothing.
            }
        }

        private void Yes_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}