﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Vinzenzmurr.Models;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VinzenzmurrApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProductDetail : Page
    {
        Product entity;
        public ProductDetail()
        {
            this.InitializeComponent();
            ApplicationViewTitleBar formattableTitleBar = ApplicationView.GetForCurrentView().TitleBar;
            formattableTitleBar.ButtonBackgroundColor = Colors.Transparent;
            CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
            coreTitleBar.ExtendViewIntoTitleBar = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            entity = e.Parameter as Product;
            var Previous_orders = entity.Orders;
            productNumber.Text = entity.Plu.ToString();
            productPlu.Text = entity.Number.ToString();
            productPrice.Text = entity.Price.ToString();
            productName.Text = entity.Name.ToString();
            ActualOrdertxt.Text = entity.Order.ToString();
            donnertxt.Text = entity.New_Mon_Order.ToString();
            freitxt.Text = entity.New_Tue_Order.ToString();
            montatxt.Text = entity.New_Wen_Order.ToString();
            Dienstxt.Text = entity.New_Thu_Order.ToString();
            Mitttxt.Text = entity.New_Fri_Order.ToString();

            tt1.Text = Previous_orders[0].Date ;
            tt2.Text = Previous_orders[1].Date ;
            tt3.Text = Previous_orders[2].Date ;
            tt4.Text = Previous_orders[3].Date ;
            tt5.Text = Previous_orders[4].Date ;


            ff1.Text = Previous_orders[0].Quantity +" " + entity.Selling_quantity.ToString();
            ff2.Text = Previous_orders[1].Quantity +" " + entity.Selling_quantity.ToString();
            ff3.Text = Previous_orders[2].Quantity +" " + entity.Selling_quantity.ToString();
            ff4.Text = Previous_orders[3].Quantity +" " + entity.Selling_quantity.ToString();
            ff5.Text = Previous_orders[4].Quantity +" " + entity.Selling_quantity.ToString();
        }
        private void back_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var newOrder = ActualOrdertxt.Text;
            if (!string.IsNullOrEmpty(newOrder))
            {
                var listtoadd = Helpers.Utils.OrderList;
                var isexistant = listtoadd.IndexOf(entity) > -1;
                if (!isexistant)
                {
                    entity.Order = Convert.ToInt32(newOrder);
                    listtoadd.Add(entity);
                }
                else
                {
                    int index = listtoadd.IndexOf(entity);
                    entity.Order = Convert.ToInt32(newOrder);
                    listtoadd[index] = entity;

                }
                var Global_Product_List = WareBestellenPage.Product_List;
                isexistant = Global_Product_List.IndexOf(entity) > -1;
                if (isexistant)
                {

                    int index = Global_Product_List.IndexOf(entity);
                    Global_Product_List[index] = entity;
                }
            }
            

        }

        private void TextBox_FocusDisengaged(Control sender, FocusDisengagedEventArgs args)
        {

        }
    }
}
