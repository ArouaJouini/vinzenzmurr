﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Vinzenzmurr.Models;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VinzenzmurrApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProductDetailsPage : Page
    {
       Product entity;
        public ProductDetailsPage()
        {
            this.InitializeComponent();
            ApplicationViewTitleBar formattableTitleBar = ApplicationView.GetForCurrentView().TitleBar;
            formattableTitleBar.ButtonBackgroundColor = Colors.Transparent;
            CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
            coreTitleBar.ExtendViewIntoTitleBar = true;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
             entity = e.Parameter as Product;
            productNumber.Text = entity.Plu.ToString();
            productPlu.Text = entity.Number.ToString();
            productPrice.Text = entity.Price.ToString();
            productName.Text = entity.Name.ToString();
            actualOrderField.Text = entity.Order.ToString();
            actualOrderFieldTxt.Text = entity.Selling_quantity.ToString();
            ordertxt.Text = entity.Selling_quantity.ToString();
            ordertxt1.Text = entity.Selling_quantity.ToString();
            ordertxt2.Text = entity.Selling_quantity.ToString();
            ordertxt3.Text = entity.Selling_quantity.ToString();
            ff1.Text = "11 " + entity.Selling_quantity.ToString();
            ff2.Text = "0 " + entity.Selling_quantity.ToString();
            ff3.Text = "7 " + entity.Selling_quantity.ToString();
            ff4.Text = "2 " + entity.Selling_quantity.ToString();
            ff5.Text = "9 " + entity.Selling_quantity.ToString();
        }
        private void back_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private void actualOrderAdd_Click(object sender, RoutedEventArgs e)
        {
            var i = Convert.ToInt32(actualOrderField.Text, CultureInfo.InvariantCulture) + 1;
            actualOrderField.Text = i.ToString();


          
            var listtoadd = Helpers.Utils.OrderList;
            var isexistant = listtoadd.IndexOf(entity) > -1;
            if (!isexistant)
            {
                entity.Order = entity.Order + 1;
                listtoadd.Add(entity);
            }
            else
            {
                int index = listtoadd.IndexOf(entity);
                entity.Order = entity.Order + 1;
                listtoadd[index] = entity;

            }
            var Global_Product_List = WareBestellenPage.Product_List;
             isexistant = Global_Product_List.IndexOf(entity) > -1;
            if (isexistant)
            {

                int index = Global_Product_List.IndexOf(entity);
                Global_Product_List[index] = entity;
            }

        }
        private void actualOrderMins_Click(object sender, RoutedEventArgs e)
        {
            double i = double.Parse(actualOrderField.Text, CultureInfo.InvariantCulture) - 1;
            if (i >= 0)
                actualOrderField.Text = i.ToString();
            else
                actualOrderField.Text = "0";

            //Delete/update the item in the list to be delivered
            var listtoadd = Helpers.Utils.OrderList;
            var isexistant = listtoadd.IndexOf(entity) > -1;
            if (isexistant)
            {
                int index = listtoadd.IndexOf(entity);
                if (entity.Order != 0)
                {
                    entity.Order = entity.Order - 1;
                    if (entity.Order == 0)
                    {
                        listtoadd.RemoveAt(index);
                    }
                }

            }

            var Global_Product_List = WareBestellenPage.Product_List;
            isexistant = Global_Product_List.IndexOf(entity) > -1;
            if (isexistant)
            {

                int index = Global_Product_List.IndexOf(entity);
                Global_Product_List[index] = entity;
            }


        }

        private void order1add(object sender, RoutedEventArgs e)
        {           
            var i = Convert.ToInt32(montagtxt.Text, CultureInfo.InvariantCulture) + 1;
            montagtxt.Text = i.ToString();
        }

        private void order1remove(object sender, RoutedEventArgs e)
        {
            var i = Convert.ToInt32(montagtxt.Text, CultureInfo.InvariantCulture) - 1;
            if (i >= 0)
                montagtxt.Text = i.ToString();
            else
                montagtxt.Text = "0";
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
          
            var i = Convert.ToInt32(dienstagtxt.Text, CultureInfo.InvariantCulture) + 1;
            dienstagtxt.Text = i.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var i = Convert.ToInt32(dienstagtxt.Text, CultureInfo.InvariantCulture) - 1;
            if (i >= 0)
                dienstagtxt.Text = i.ToString();
            else
                dienstagtxt.Text = "0";
        }
        
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            
            var i = Convert.ToInt32(mitwoodtxt.Text, CultureInfo.InvariantCulture) + 1;
            mitwoodtxt.Text = i.ToString();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var i = Convert.ToInt32(mitwoodtxt.Text, CultureInfo.InvariantCulture) - 1;
            if (i >= 0)
                mitwoodtxt.Text = i.ToString();
            else
                mitwoodtxt.Text = "0";
        }
        
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {

          
            var i = Convert.ToInt32(donnerstagtxt.Text, CultureInfo.InvariantCulture) + 1;
            donnerstagtxt.Text = i.ToString();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            var i = Convert.ToInt32(donnerstagtxt.Text, CultureInfo.InvariantCulture) - 1;
            if (i >= 0)
                donnerstagtxt.Text = i.ToString();
            else
                donnerstagtxt.Text = "0";
        }

      
    }
}
